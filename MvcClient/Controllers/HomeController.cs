﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MvcClient.Models;

namespace MvcClient.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            //var ru = new RequestUrl("http://localhost:5000/connect/authorize");

            //var url = ru.CreateAuthorizeUrl(
            //    clientId: "mvc",
            //    responseType: "id_token",
            //    redirectUri: "https://localhost:5005/signin-oidc",
            //    responseMode: "form_post",
            //    acrValues: "idp:Google",
            //    nonce: "636939487073733482.NDE1MGU5OTQtYWI0Ny00ZTU1LTg3YTQtNjllM2Y1MGY5MjlmMWQxYzgzYmYtNzYwYi00M2JhLTgyZWYtNGExNWYyYWYxOTAz",
            //    scope: "openid profile");

            //return Redirect(url);

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
